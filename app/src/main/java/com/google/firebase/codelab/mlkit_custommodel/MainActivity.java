package com.google.firebase.codelab.mlkit_custommodel;
import android.content.pm.*;
import android.os.*;
import android.util.*;
import android.view.*;
import com.google.android.gms.tasks.*;
import com.google.firebase.ml.vision.*;
import com.google.firebase.ml.vision.common.*;
import com.google.firebase.ml.vision.label.*;
import com.microsoft.appcenter.*;
import com.microsoft.appcenter.analytics.*;
import com.microsoft.appcenter.crashes.*;
import com.otaliastudios.cameraview.*;
import java.util.*;
import com.google.android.gms.tasks.Task;
import android.app.*;


public class MainActivity extends Activity
{
	CameraView camera;

	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		camera = findViewById(R.id.camera);
		AppCenter.start(getApplication(), "d96bbc76-22c8-4236-9221-acd3742783ce", Analytics.class, Crashes.class);
		View decor = getWindow().getDecorView();
		decor.setSystemUiVisibility(
			View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|
			View.SYSTEM_UI_FLAG_FULLSCREEN|
			View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
		);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
	}

	@Override
	protected void onResume()
	{
		// TODO: Implement this method
		super.onResume();
		camera.start();
		camera.addFrameProcessor(new FrameProcessor(){

				@Override
				public void process(Frame frame)
				{
					byte[] data = frame.getData();
					int rotation = frame.getRotation();
					//classify(data,rotation);
				}
			});
		
	}

	@Override
	protected void onPause()
	{
		// TODO: Implement this method
		super.onPause();
		camera.stop();
		
	}
	
	private void classify(byte [] data, int rotation){
		FirebaseVisionImageMetadata metadata = new FirebaseVisionImageMetadata.Builder()
			.setWidth(480)   // 480x360 is typically sufficient for
			.setHeight(360)  // image recognition
			.setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
			.setRotation(rotation)
			.build();
		FirebaseVisionImage image = FirebaseVisionImage.fromByteArray(data, metadata);
		// Or: FirebaseVisionImage image = FirebaseVisionImage.fromByteArray(byteArray, metadata);
		FirebaseVisionLabelDetector detector = FirebaseVision.getInstance()
			.getVisionLabelDetector();
		Task<List<FirebaseVisionLabel>> result = detector.detectInImage(image)
			.addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionLabel>>(){

				@Override
				public void onSuccess(List<FirebaseVisionLabel> p1)
				{
					Log.i("Fire","got resutl" + p1);
				}
			}).
			addOnFailureListener(
			new OnFailureListener() {
				@Override
				public void onFailure(@NonNull Exception e) {
					Log.d("error",e.toString());
				}
			});
}}
